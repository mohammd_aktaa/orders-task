@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                @component('components.table_grid.table', ['title' => $title, 'url' => $url,
                        'gridName' =>$gridName, 'searchTooltip' => 'Search for Name, Number, Client Name'])
                @endcomponent

            </div>
        </div>
    </div>
    @component('components.fragments.orders.create_modal')
    @endcomponent
@stop
@section('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/datatables/datatables.bundle.css')}}">
@endsection
@section('scripts')
    <script src="{{asset('assets/plugins/datatables/datatables.bundle.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/datatables_init.js')}}"></script>
    <script src="{{asset('js/orders/index.js')}}"></script>
    <script src="{{asset('js/crud.actions.js')}}"></script>
@endsection
