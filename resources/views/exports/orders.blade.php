<table class="table table-striped">
    <thead>
    <tr>
        <th>Name</th>
        <th>Number</th>
        <th>Date</th>
        <th>Client</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <td>{{ $order->name }}</td>
            <td>{{ $order->uuid }}km</td>
            <td>{{ $order->date }}</td>
            <td>{{ $order->client->name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
