<?php

namespace App\Http\Controllers;

use App\Exports\OrdersExport;
use App\Http\Requests\AddUpdateOrderRequest;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Order::loadAll();

            return response()->json($data);
        }
        $gridName = 'orders';
        $title = 'Orders';
        $url = 'orders';

        return view('orders.index', compact('gridName', 'title', 'url'));
    }

    public function destroy($order)
    {
        $item = Order::findOrFail($order);
        $item->delete();

        return response()->json(['message' => 'Successfully Deleted!']);
    }

    public function store(AddUpdateOrderRequest $request)
    {
        $data = $request->validated();
        $data['uuid'] = Str::uuid();

        Order::create($data);

        return response()->json(['message' => 'Successfully Added!']);
    }

    public function update($order, AddUpdateOrderRequest $request)
    {
        $data = $request->validated();
        $orderItem = Order::findOrFail($order);
        $orderItem->update($data);

        return response()->noContent();
    }

    public function export()
    {
        return Excel::download(new OrdersExport(), 'orders-'.date('Y-m-d-h-m').'.xlsx');
    }
}
