<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function clientsAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = \App\Models\Client::whereRaw('upper(name) like upper(\'%' . $q . '%\')')->take(20)
            ->get(['name as text', 'id as id']);

        return ['items' => $data];
    }
}
