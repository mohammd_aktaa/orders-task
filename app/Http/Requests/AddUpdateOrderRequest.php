<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class AddUpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'uuid' => ['sometimes', 'unique:orders,uuid,' . $this->segment(2)],
            'date' => ['required', 'date'],
            'client_id' => ['required', 'numeric', 'exists:clients,id'],
        ];
    }

}
