<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrdersExport implements FromView, ShouldAutoSize
{
    use Exportable;


    /**
     * @return View
     */
    public function view(): View
    {
        $orders = Order::with('client')->get();
        return view('exports.orders', [
            'orders' => $orders
        ]);
    }
}
