<?php

namespace App\Models;

use App\Traits\DataViewer;

class Order extends \Eloquent
{
    protected $fillable = [
        'name',
        'uuid',
        'date',
        'client_id',
    ];

    public static function loadAll($export = false)
    {
        $query = self::select(['orders.id', 'orders.name', 'orders.date', 'orders.uuid', 'orders.client_id', 'clients.name as client_name'])
            ->leftJoin('clients', 'orders.client_id', '=', 'clients.id');

        return DataViewer::searchDataGrid($query, ['orders.name', 'orders.uuid', 'clients.name'], $export);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
