<?php

namespace App\Traits;

trait DataViewer
{
    public static function searchDataGrid($query, $quickSearchColumns, $export = false)
    {
        $request = app()->make('request');
        $search = $request->input('search');
        $dataOrder = $request->input('order');
        $response = [
            'draw' => $request->input('draw', 1),
            'recordsTotal' => $query->count(),
            'recordsFiltered' => 0,
            'aaData' => [],
        ];
        $operators = [
            'eq' => '=', 'neq' => '!=',
            'lt' => '<', 'lte' => '<=',
            'gt' => '>', 'gte' => '>=',
            'd_eq' => '=', 'd_neq' => '!=',
            'd_lt' => '<', 'd_lte' => '<=',
            'd_gt' => '>', 'd_gte' => '>=',
            'between' => '>=',
            'startswith' => 'like', 'endswith' => 'like', 'contains' => 'like',
            'in', 'not_in', 'empty', 'not_empty',
        ];
        // Quick Search
        if (!empty($search['value'])) {
            $query->where(function ($qry) use ($quickSearchColumns, $search) {
                foreach ($quickSearchColumns as $col) {
                    $qry->orWhere($col, 'like', '%'.$search['value'].'%');
                }
            });
        }
        //Filters
        if (null != $request->input('filters') && !empty($request->filters['filter'])) {
            $filters = $request->filters['filter'];
            foreach ($filters['value'] as $filterKey => $filterValue) {
                switch ($filters['operator'][$filterKey]) {
                    case 'eq':
                    case 'neq':
                    case 'lt':
                    case 'lte':
                    case 'gt':
                    case 'gte':
                        !empty($filterValue) || ('' != $filterValue & 0 == $filterValue) ?
                            $query->where(
                                $filters['column'][$filterKey],
                                $operators[$filters['operator'][$filterKey]],
                                $filterValue
                            )
                            : null;
                        break;
                    case 'd_eq': // Date Operators
                    case 'd_neq':
                    case 'd_lt':
                    case 'd_lte':
                    case 'd_gt':
                    case 'd_gte':
                    case 'between':
                        !empty($filterValue) ?
                            $query->whereDate(
                                $filters['column'][$filterKey],
                                $operators[$filters['operator'][$filterKey]],
                                (string) $filterValue
                            )
                            : null;
                        break;
                    case 'startswith':
                        !empty($filterValue) ?
                            $query->where(
                                $filters['column'][$filterKey],
                                $operators[$filters['operator'][$filterKey]],
                                (string) $filterValue.'%'
                            )
                            : null;
                        break;
                    case 'endswith':
                        !empty($filterValue) ?
                            $query->where(
                                $filters['column'][$filterKey],
                                $operators[$filters['operator'][$filterKey]],
                                '%'.(string) $filterValue
                            )
                            : null;
                        break;
                    case 'contains':
                        !empty($filterValue) ?
                            $query->where(
                                $filters['column'][$filterKey],
                                $operators[$filters['operator'][$filterKey]],
                                '%'.(string) $filterValue.'%'
                            )
                            : null;
                        break;
                    case 'in':
                        !empty($filterValue) ? $query->whereIn($filters['column'][$filterKey], $filterValue) : null;
                        break;
                    case 'not_in':
                        !empty($filterValue) ? $query->whereNotIn($filters['column'][$filterKey], $filterValue) : null;
                        break;
                    case 'empty':
                        $query->whereNull($filters['column'][$filterKey]);
                        break;
                    case 'not_empty':
                        $query->whereNotNull($filters['column'][$filterKey]);
                        break;
                }
            }
        }
        $response['recordsFiltered'] = $query->count();

        //Order By
        if (!empty($dataOrder)) {
            foreach ($dataOrder as $order) {
                $query->orderBy($request->columns[$order['column']]['data'], $order['dir']);
            }
        }
        !$export ? $query->skip($request->input('start', 0))->take($request->input('length', 10)) : false;
        $response['aaData'] = $query->get();

        return $response;
    }
}
