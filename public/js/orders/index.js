var _orders = [];
var _columnDefs = [{
    className: "dt-actions",
    "orderable": false,
    "targets": [0]
}];

let clientSelect = $('.select2').select2({
    placeholder: $(this).attr('data-placeholder'),
    minimumResultsForSearch: 0,
    allowClear: true,
    width: '100%',
    ajax: {
        url: '/clients/autocomplete',
        dataType: 'json',
        delay: 400,
        data: function (params) {
            var param = (typeof this.attr('data-param') !== typeof undefined) ? this.attr('data-param') : null;
            if (param && param.charAt(0) === '#') {
                var name = $(param).attr('name') || $(param).attr('id');
                param = JSON.parse('{"' + name + '":"' + $(param).val() + '"}');
            } else if (param)
                param = JSON.parse('{"' + param.replaceAll("&", "\",\"").replaceAll("=", "\":\"") + '"}');
            /*if(param && param.charAt(0) === '.') {

             }*/
            var $data = {q: params.term, page: params.page};
            if (param) {
                $data = $.extend($data, param);
            }
            return $data;
        },
        processResults: function (data, params) {
            params.page = params.page || 1;
            return {
                results: data.items,
                pagination: {
                    more: (params.page * 30) < data.total_count
                }
            };
        },
        cache: true
    },
});

$.ajax({
    type: 'GET',
    url: '/clients/autocomplete'
}).then(function (data) {
    $.each(data.items,function (index,item) {
        let option = new Option(item.text, item.id, false, false);
        clientSelect.append(option).trigger('change');
    })
});
_fnRowCallback = function (nRow, aData, iDisplayIndex) {
    _orders[aData['id']] = aData;
    return nRow;
};
var _columns = [{
    mData: 'id',
    sTitle: '',
    sType: 'html',
    bSearchable: false,
    bSortable: false,
    render: function (data, type, row, meta) {
        return `<div class="dropdown d-inline">
                           <a class="dropdown-toggle actions" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-bars fa-lg"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                               <a class="dropdown-item" href="javascript:"  onclick="editDetails(${row['id']})">
                                Edit Details
                               </a>
                               <a class="dropdown-item" href="javascript:" onclick="deleteItem(${row['id']})">
                                Delete
                               </a>
                           </div>
                        </div>
        `;
    }
},
    {
        mData: 'uuid',
        sTitle: 'Order Number',
        sType: 'text',
        bSearchable: false,
    },
    {
        mData: 'name',
        sTitle: 'Order Name',
        sType: 'text',
        bSearchable: false,
    },
    {
        mData: 'date',
        sTitle: 'Order Date',
        sType: 'text',
        bSearchable: false,
    },
    {
        mData: 'client_name',
        sTitle: 'Client Name',
        sType: 'text',
        bSearchable: false,
    },
];

openAdd = () => {
    let $modal = $('#add-modal');
    let $form = $('.add-form');
    clearForm($form);
    $form.find('input[name="_method"]').remove();
    $form.attr('action', `orders`);
    $form.find('input[type="password"]').prop('readonly', false);
    $modal.find('.modal-title').text(`Add Order`);
    clearErrors($modal);
    $modal.modal('show');
};

editDetails = (id) => {
    let $order = _orders[id];
    let $modal = $('#add-modal');
    let $form = $('.add-form');
    clearForm($form);
    $form.append('<input type="hidden" name="_method" value="PATCH">');
    $form.attr('action', `orders/${id}`);
    $form.find('input[type="password"]').prop('readonly', true);
    $modal.find('.modal-title').text(`Edit Order ${$order['name']}`);
    clearErrors($modal);
    _fill($modal, $order);
    $modal.modal('show');
};

deleteItem = (id) => {
    deleteOpr(id, `orders/${id}`);
};
$('.add-form').on('submit', function (e) {
    e.preventDefault();

    saveItem(e, this, $(this).attr('action'));
});
