//initializing
$(function () {
    "use strict";
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
});

function showAlert(options = null, onConfirm, onCancel) {
    let defaultOptions = {
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        // confirmButtonText: 'Yes, delete it!',
        // customClass: {
        //     confirmButton: "btn btn-primary",
        //     cancelButton: "btn btn-default"
        // }
    };
    let $options = $.extend(defaultOptions, options);
    swal.fire($options).then(function (result) {
        if (result) {
            if (onConfirm)
                onConfirm();
        } else {
            if (onCancel)
                onCancel();
        }
    });
}

function clearForm($content) {
    $($content).find('.form-control').val('');
    $($content).find('.select2').val(null).trigger('change');
}

function clearErrors($content) {
    $content.find('.error').remove();
}

function _fill($cont, obj) {
    $($cont).find(':input:not(".select2,[name=_method],[name=_token]")').each(function () {
        var $this = $(this);
        if ($this.prop('type')) {
            if ($this.prop('type') === 'radio') {
                $this.prop('checked', obj[$(this).attr('name')] == $(this).attr('value'));
            } else if ($this.prop('type') === 'checkbox') {
                $this.prop('checked', (obj[$(this).attr('name')] === '1' || obj[$(this).attr('name')] === 1));
            } else {
                if ($(this).attr('name') && $(this).attr('name').includes('[')) {
                    let [$obj, name] = $(this).attr('name').replace(']', '').split('[');
                    $this.val(obj[$obj] && obj[$obj][name]);
                } else
                    $this.val(obj[$(this).attr('name')]);
            }
        } else
            $this.html(obj[$(this).attr('name')]);
    });
    $($cont).find('.select2').each(function () {
        var $this = $(this), name = $this.attr('name');
        if ($this.attr('multiple')) {
            var selectedValues = obj[$this.attr('name').replace('[', '').replace(']', '')];
            $this.val(selectedValues).trigger('change');
        } else if (obj[$this.attr('name')] != null) {
            console.log(obj[$this.attr('name')])
            $this.val(obj[$this.attr('name')]).trigger('change');
        }
    });
    $($cont).find('.kt-selectpicker').each(function () {
        var $this = $(this), name = $this.attr('name');
        if (obj[$this.attr('name')] != null) {
            $this.val(obj[$this.attr('name')]).trigger('change');
        }
    });
}


function ucwords(str) {
    return (str + '').replace(/-/g, ' ').replace(/_/g, ' ').replace(/^(.)|\s+(.)/g, function ($1) {
        return $1.toUpperCase();
    });
}

HandleJsonErrors = function (xhr, errorThrown) {
    switch (xhr.status) {
        case 400:
            if (typeof xhr.responseJSON.message === 'string') {
                toastr.error(xhr.responseJSON.message, 400);
            }
            if (typeof xhr.responseJSON.message === 'object') {
                _message = '<div class="text-left">';
                for (let error in xhr.responseJSON.message) {
                    _message += xhr.responseJSON.message[error];
                    let input;
                    if (error && error.includes('.')) {
                        let $error = error.replace('.', '[') + ']';
                        input = $("[name='" + $error + "']");

                    } else
                        input = $('[name=' + error + ']');
                    let errorCont = input.closest('span.error');
                    if (errorCont.length) {
                        errorCont.text(xhr.responseJSON.message[error][0]);
                    } else {
                        if (input.hasClass('select2')) {
                            input.parent().find('.select2-container').after('<span class="text-danger error">' + xhr.responseJSON.message[error][0] + '</span>');
                        } else
                            input.after('<span class="text-danger error">' + xhr.responseJSON.message[error][0] + '</span>');
                    }
                }
                _message += '</div>';

                toastr.error(_message, '400', {
                    timeOut: 3000
                });
            }
            break;
        case 401:
            toastr.warning(xhr.responseJSON.message, '401');
            if (window.location.pathname !== '/login') {
                setTimeout(function () {
                    window.location = getBaseURL() + 'login';
                }, 1500);
            }
            break;
        case 404:
            toastr.error(xhr.responseJSON.message, '404');
            break;
        case 422:
            _message = '<div class="text-left">';
            for (let error in xhr.responseJSON.errors) {
                _message += xhr.responseJSON.errors[error][0];
                let input;
                if (error && error.includes('.')) {
                    let $error = error.replace('.', '[') + ']';
                    input = $("[name='" + $error + "']");
                } else {
                    input = $('[name=' + error + ']');
                }
                let errorCont = input.closest('span.error');
                if (errorCont.length) {
                    errorCont.text(xhr.responseJSON.errors[error][0]);
                } else {
                    if (input.hasClass('select2')) {
                        input.parent().find('.select2-container').after('<span class="text-danger error">' + xhr.responseJSON.errors[error][0] + '</span>');
                    } else if (input.parent().hasClass('input-group')) {
                        input.parent().after('<span class="text-danger error">' + xhr.responseJSON.errors[error][0] + '</span>');
                    } else
                        input.after('<span class="text-danger error">' + xhr.responseJSON.errors[error][0] + '</span>');
                }
            }
            _message += '</div>';
            toastr.error(_message, '422', {
                timeOut: 5000
            });
            break;
        case 500:
            toastr.error(xhr.responseJSON.message, '500');
            break;
        default:
            toastr.error('Something went wrong: ' + errorThrown, 'Oops..');
    }
};
