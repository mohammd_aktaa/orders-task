/* global HandleJsonErrors, ـaoColumns*/

var $dtProcessing = jQuery('#dt_processing');
var $gridColumnsList = jQuery('#grid-columns-list');
var $dropdownToggle = jQuery('body .dropdown-toggle');
var $toDateFld = jQuery('.toDate');
var $hideShowColumnsList = jQuery('.hide-show-columns');
//Table Grid
var tableGrid;
var $tableGridObj = jQuery('.table-grid');
var tableGridData = $tableGridObj.data();
var tableGridDataSet;
//Filters
var $filtersContainer = jQuery('#filters');
var filters = [];
var $filtersForm = jQuery('#filtersForm');
jQuery(document).ready(function () {
    // Initialise DataTable
    tableGrid = $tableGridObj.on('processing.dt', function (e, settings, processing) {
        // $dtProcessing.css('display', processing ? 'block' : 'none');
    }).DataTable({
        columns: window._aoColumns,
        stateSave: true,
        scrollX: true,
        processing: false,
        serverSide: true,
        ordering: true,
        regex: false,
        orderMulti: true,
        displayStart: 0,
        paging: true,
        pagingType: "simple_numbers",
        pageLength: 10,
        responsive: false,
        sAjaxDataProp: 'aaData',
        language: {
            emptyTable: '<div class="no-data-to-display"><i class="fa fa-exclamation-triangle"></i></br>No data to display</div>',
            zeroRecords: '<div class="no-data-to-display"><i class="fa fa-exclamation-triangle"></i></br>No matching records found</div>',
            sLengthMenu: 'Show _MENU_ entries',
            sInfo: 'Showing _START_ to _END_ of _TOTAL_ entries',
            sInfoEmpty: 'Showing 0 to 0 of 0 entries',
            sInfoFiltered: '(filtered from _MAX_ total entries)',
            sLoadingRecords: 'Loading...',
            sSearch: 'Search: ',
            oPaginate: {
                sFirst: 'First',
                sPrevious: 'Last',
                sNext: 'Next',
                sLast: 'Previous'
            }
        },
        // stateLoadParams: function (settings, data) {
        //     if (!jQuery.isEmptyObject(data.columns)) {
        //         for (var col in data.columns) {
        //             data.columns[col]['visible'] = data.columns[col]['visible'] === 'true';
        //             window._aoColumns[col].sType !== 'html' ?
        //                 $hideShowColumnsList.append('<a class="dropdown-item" href="javascript:;" data-column="' + col + '"><i class="' + (data.columns[col]['visible'] ? 'far fa-check-square' : 'far fa-square') + '"></i> ' + window._aoColumns[col].sTitle + '</a>') :
        //                 false;
        //         }
        //     }
        //     if (!jQuery.isEmptyObject(data.filters)) {
        //         for (var i in data.filters.filter.value) {
        //             if (data.filters.filter.value[i] !== '' && data.filters.filter.value[i] !== null) {
        //                 jQuery('[name="filter.value"]:eq(' + i + ')').val(data.filters.filter.value[i]).removeAttr('readonly');
        //                 jQuery('[name="filter.operator"]:eq(' + i + ')').val(data.filters.filter.operator[i]);
        //             }
        //         }
        //     }
        //     checkFiltersIfExists();
        //     return data;
        // },
        // stateSaveCallback: function (settings, data) {
        //     jQuery.ajax({
        //         url: getBaseURL() + "grid_preferences",
        //         data: {
        //             'key_name': tableGridData.gridName,
        //             'key_value': data,
        //             'url': 'grid_preferences'
        //         },
        //         dataType: "JSON",
        //         type: "POST",
        //         complete: function (response) {
        //             if (!response.status) {
        //                 toastr.error('Something went wrong while saving grid preferences, try to reset the grid', ''), {
        //                     timeOut: 3500
        //                 };
        //             }
        //         },
        //         error: HandleJsonErrors
        //     });
        // },
        // stateLoadCallback: function () {
        //     var o;
        //     jQuery.ajax({
        //         url: getBaseURL() + 'grid_preferences',
        //         dataType: "JSON",
        //         type: "GET",
        //         async: false,
        //         data: {
        //             'grid': tableGridData.gridName
        //         },
        //         success: function (response) {
        //             o = response.data;
        //         },
        //         error: HandleJsonErrors
        //     });
        //     if (!jQuery.isEmptyObject(o)) {
        //         filters = o.filters;
        //     }
        //     return o;
        // },
        ajax: {
            url: BASE_URL + tableGridData.url,
            type: "GET",
            dataType: "JSON",
            dataSrc: 'aaData',
            data: function (d) {
                d.url = tableGridData.gridName;
                d.filters = filters;
            },
            beforeSend: function () {
                checkFiltersIfExists();
            },
            complete: function () {
                $dtProcessing.css('display', 'none');
                // if (isTouchDevice() === true) {
                //     jQuery("[data-toggle='tooltip']").tooltip('disable');
                // }
            },
            error: HandleJsonErrors
        },
        aoColumns: window._columns,
        aoColumnDefs: window._columnDefs,
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            return _fnRowCallback(nRow, aData, iDisplayIndex);
        },
        fnServerParams: function (aoData) {
            aoData['filters'] = filters;
            tableGridDataSet = aoData;
        },
        // initComplete: function (settings, json) {
        //     if (isEmpty($hideShowColumnsList.html())) {
        //         for (var col in window._aoColumns) {
        //             window._aoColumns[col]['visible'] = 'true';
        //             window._aoColumns[col].sType !== 'html' ?
        //                 $hideShowColumnsList.append('<a class="dropdown-item" href="javascript:;" data-column="' + col + '"><i class="far fa-check-square"></i> ' + window._aoColumns[col].sTitle + '</a>') :
        //                 false;
        //         }
        //     }
        // }
    });

    // Fix dropdown overflow property
    jQuery('.table-responsive').on('show.bs.dropdown', function () {
        jQuery('.table-responsive').css("overflow", "inherit");
        jQuery('.dataTables_scrollBody').css("overflow", "inherit");
    });

    jQuery('.table-responsive').on('hide.bs.dropdown', function () {
        jQuery('.table-responsive').css("overflow", "auto");
        jQuery('.dataTables_scrollBody').css("overflow", "auto");
    })

    // Grab the datatables input box and alter how it is bound to events ,Add Quick Search Tooltip
    jQuery('.dataTables_filter input')
        .attr('data-toggle', 'tooltip')
        .attr('data-placement', 'bottom')
        .attr('title', tableGridData.searchTooltip)
        .unbind() // Unbind previous default bindings
        .bind("input", function (e) { // Bind our desired behavior
            if (this.value.length >= 3) { // Search if the length is 3 or more characters
                tableGrid.search(this.value).draw();
            }
            if (this.value === "") { // Ensure we clear the search if they backspace far enough
                tableGrid.search("").draw();
            }
            return;
        });
    // Bootstrap Dropdown Issue With DataTable
    $dropdownToggle.dropdown();
    //Bootstrap Tooltip Data Toggle
    jQuery('[data-toggle="tooltip"]').tooltip();
    // Highlight Table TR
    jQuery('table.dataTable tbody').on('click', 'td:not(".dt-actions")', function () {
        var _parentTr = jQuery(this).parent();
        _parentTr.hasClass('highlighted') ? _parentTr.removeClass('highlighted') : _parentTr.addClass('highlighted');
    });
    // jQuery('.input-daterange').datepicker(getDatepickerOptions());
});
// Events/Listeners
$gridColumnsList.on('click', 'a', function (e) {
    e.stopPropagation();
    e.preventDefault();
    // Get the column API object
    var column = tableGrid.column(jQuery(this).data('column'));
    tableGrid.columns([jQuery(this).attr('data-column')]).visible(!column.visible()); // Toggle the visibility
    jQuery('i', this).attr('class', column.visible() ? 'far fa-check-square' : 'far fa-square'); // Toggle column visibility icon
    tableGrid.draw();
});
$filtersForm.keypress(function (e) {
    if (e.which === 13 && $filtersContainer.is(":visible") === true) {
        e.preventDefault();
        prepFilters();
    }
});
jQuery('.daterange').change(function () {
    var _toDate = jQuery('.toDate', jQuery(this).parent().parent());
    jQuery(this).val() === 'between' ?
        _toDate.prop('readonly', false) :
        _toDate.prop('readonly', true).val('');
});
jQuery('input[type="reset"]', $filtersContainer).on('click', function () {
    filters = [];
    $filtersForm[0].reset();
    $filtersContainer.is(":visible") === true ? toggleFilters() : true;
    $toDateFld.prop('readonly', true).val('');
    reloadDataGrid();
});
jQuery(window).on('load', function () {
    jQuery('a', $hideShowColumnsList).on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var column = tableGrid.column(jQuery(this).data('column'));
        tableGrid.columns([jQuery(this).attr('data-column')])
            .visible(!column.visible(), false); // Toggle the visibility
        jQuery('i', this).attr('class', column.visible() ? 'far fa-check-square' : 'far fa-square'); // Toggle column visibility icon
        tableGrid.draw();
    });
});
//Functions

function toggleFilters() {
    $filtersContainer.slideToggle({
        duration: '300'
    });
}

function prepFilters() {
    jQuery('input', $filtersForm).each(function () {
        jQuery(this).attr('readonly') ? jQuery(this).val('') : null;
    });
    filters = $filtersForm.serializeObject();
    toggleFilters();
    reloadDataGrid();
}

function checkFiltersIfExists() {
    _existingFilters = false;
    jQuery('[name="filter.value"]', $filtersForm).each(function () {
        _existingFilters = !jQuery.isEmptyObject(this.value);
        if (_existingFilters)
            return false;
    });
    _existingFilters ? jQuery('.advanced-filters').addClass('existing-filters') : jQuery('.advanced-filters').removeClass('existing-filters');
}

function reloadDataGrid(pagination = false) {
    if (pagination) {
        tableGrid.ajax.reload(null, false);
    } else {
        tableGrid.ajax.reload();
    }
    jQuery('html, body').animate({
        scrollTop: jQuery(".card-header").offset().top - 100
    }, 1000);
}

function resetDataGridOptions() {
    filters = [];
    $toDateFld.attr('readonly', 'readonly').val('');
    tableGrid.columns().visible(true);
    jQuery('i', jQuery('a', $gridColumnsList)).attr('class', 'far fa-check-square');
    var oSettings = $tableGridObj.dataTable().fnSettings();
    oSettings.oPreviousSearch['sSearch'] = '';
    oSettings._iDisplayLength = 10;
    tableGrid.search("");
    jQuery('select', 'div.dataTables_length').val('10');
    reloadDataGrid();
}
