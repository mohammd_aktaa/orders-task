function saveItem(e, $this, url) {
    e.preventDefault();
    clearErrors($($this));
    let $data = new FormData($this);
    let $url = BASE_URL + url;
    jQuery.ajax({
        type: 'POST',
        url: $url,
        data: $data,
        contentType: false,
        processData: false,
        beforeSend() {
            jQuery('body').append('<div class="overlay"><i class="fa fa-spinner fa-5x fa-spin mt-5"></i></div>');
        },
        success(xhr) {
            if (xhr && xhr.message)
                toastr.success(xhr.message);
            else
                toastr.success('Operation done Successfully!');

            $('.modal').modal('hide');
            clearForm($($this));
            if (xhr && xhr.redirect) {
                window.location.replace(xhr.redirect);
            }
            reloadDataGrid(true);
        },
        error: HandleJsonErrors.bind($this),
        complete() {
            jQuery('body .overlay').remove();
        }
    });
}

function deleteOpr(id, $url, $options = null) {
    showAlert($options, () => {
        $.ajax({
            type: 'POST',
            url: BASE_URL + $url,
            data: {_method: 'DELETE', _token},
            success(xhr) {
                toastr.success(xhr.message);
                reloadDataGrid(true);
            },
            error: HandleJsonErrors
        });
    });
}


