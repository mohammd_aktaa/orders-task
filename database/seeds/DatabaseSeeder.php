<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Client::insert([
            [
                'name' => 'Ali',
            ],
            [
                'name' => 'Mohammad',
            ],
            [
                'name' => 'Abdo',
            ],
            [
                'name' => 'Mahmoud',
            ],
            [
                'name' => 'Muhannad',
            ],
            [
                'name' => 'Majd',
            ],
            [
                'name' => 'Alaa',
            ],
            [
                'name' => 'Rami',
            ],
        ]);

        \App\User::create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
    }
}
